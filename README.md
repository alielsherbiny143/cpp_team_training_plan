# C++ team training plan

You learn about each topic from the suggested resources or any other resources you may like.

There should be a mini-quiz discussion after you complete each topic.

## Orientation Session
**Estimated Duration: 4 hours.**

* People
* Training Plan
* Servers
* Projects
* Memes (Culture)
* Workflow

## C keywords and concepts
Estimated Duration: 2 Days.

### Topics to learn:
* extern
* static
* name mangling
* function pointers for callbacks, void pointer as argument for context.
* C Interfaces (pass opaque struct pointer for context)
* PIMPL pattern.
* Polymorphism in C

### Quiz
* Why do we need callback functions?
* Write a small project example that uses polymorphism in C language.

### Resources that can help:
* https://www.cprogramming.com/tutorial/function-pointers.html
* https://www.codeproject.com/Articles/108830/Inheritance-and-Polymorphism-in-C
* https://chris-wood.github.io/2016/02/12/Polymorphism-in-C.html
* https://blog.stratifylabs.co/device/2019-08-05-Callbacks-in-Cpp/
* https://www.bfilipek.com/2018/01/pimpl.html

## Move Semantics
Estimated Duration: 1 Day.

### Topics to learn:
* What is an r-value?
* Universal Reference
* Perfect Forwarding
* `std::move()` and `std::forward()` and how they are implemented.

### Resources that can help:
* [Chapter M in learncpp.com](http://www.learncpp.com/) can be used to learn both Smart Pointers and Move Semantics
* [Back to Basics: Move Semantics Part 1](https://youtu.be/St0MNEU5b0o)
* [The Nightmare of Move Semantics for Trivial Classes](https://www.youtube.com/watch?v=PNRju6_yn3o)

## Templates
Estimated Duration: 1 Day.

### Topics to learn:
* Function Templates
* Class Templates
* Variadic Template
* Rules for Template Type Deduction
* Template Specialization
* Partial Template Specialization

### Resources that can help:
* [Chapter 19 in learncpp.com](http://www.learncpp.com/)
* [Template Normal Programming - Part 1](https://www.youtube.com/watch?v=vwrXHznaYLA)
* [Template Normal Programming - Part 2](https://www.youtube.com/watch?v=VIz6xBvwYd8)

### Quiz
* If we pass a const string& to a template function that takes T&& what will T resolve into?

## Smart Pointers
Estimated Duration: 4 Hours.

### Topics to learn:
* `std::unique_ptr`
* `std::shared_ptr`
* `std::weak_ptr`
* How each of the above is implemented
* Ownership Semantics

### Resources that can help:
* [Chapter M in learncpp.com](http://www.learncpp.com/) can be used to learn both Smart Pointers and Move Semantics
* [Leak Freedom by Herb Sutter](https://www.youtube.com/watch?v=JfmTagWcqoE)

### Quiz
* What is the difference between an rvalue reference and a universal reference?

## Parallel Programming
Estimated Duration: 2.5 Days.

### Topics to learn:
* std::thread
* std::mutex
* std::atomic
* OpenMP
* Thread Pool

### Bonus
* std::future, std::promise
* Consumer/Producer Pattern

### Resources that can help:
* [Concurrent Programming with C++ 11](https://www.youtube.com/playlist?list=PL5jc9xFGsL8E12so1wlMS0r0hTQoJL74M)

### Quiz
* What does the volatile keyword mean in C++? Can you think of a situation where you'd need it?
* What's the difference between busy waiting and idle waiting?

## Linux Command Line Basics
Estimated Duration: 1 Day.

### Topics to learn:
* ls
* pwd
* cd
* cp
* mv
* rm
* man
* echo
* cat
* mkdir
* rmdir
* touch
* grep
* chmod
* chown
* sudo
* Piping
* Redirection
* nano
* vim

### Resources that can help:
* [Bucky's Linux Tutorial for Beginners](https://www.youtube.com/playlist?list=PL6gx4Cwl9DGCkg2uj3PxUWhMDuTw3VKjM)

### Quiz
* What is a command?
* How can I create my own command?
* In chmod what does the 764 permission mean?
* How do I change the ownership of a folder recursively to the user sob7y?
* How to append the output of a command to a file?

## Git
Estimated Duration: 6 Hours.

### Topics to learn:
* Installation
* clone
* init
* status
* add
* commit
* pull
* push
* Conflict Resolution
* Remotes
* branch
* checkout
* merge
* reset
* revert

### Resources that can help:
* We'll have a nice, practical session isa.
* Here's a [cheatsheet](https://github.com/Open-Source-Community/iusegit/blob/master/Cheatsheet.md) to refer to when in need.

### Quiz
* How do we go back to a previous commit?
* When do conflicts occur? How do you fix them?
* What would you do if you accidentally push your password into a remote repository?
* What are some of the git workflows? Which one do you think works best for us?

## Build Process
Estimated Duration: 1 Day.

### Topics to learn:
* What is preprocessing?
* What is compilation?
* What is linking?
* Static linking
* Dynamic linking
* PIC and PIE

### Resources that can help:
* https://stackoverflow.com/a/33690144/3659011
* https://cirosantilli.com/elf-hello-world
* https://stackoverflow.com/a/51308031/3659011

### Quiz
* What's the difference between an object file and an executable?
* What is a static library?
* What does the -fPIC flag mean?
* How to see if a symbol is defined or not in a library?
* How to see the dependencies of a library?
* Where are libs stored in Linux?
* Where are programs stored in Linux?
* What is the difference between a shared library and an executable?
* Write a program that doesn't link to libc

## Make toolchains
Estimated Duration: 3 Hours.

### Topics to learn:
* Writing simple Makefiles
* Writing simple CMake files

### Resources that can help:
* [Makefile](https://www.youtube.com/watch?v=_r7i5X0rXJk)

### Quiz
* What is .PHONY used for?
* How to set a variable in Make?
* What is a rule in a Makefile?
* How to run concurrent jobs in make?
* How to manage rule dependencies in make?



## CMake
Estimated Duration: 20 hours.

### Topics to learn:
- Part 1:
    - What is a building system ?
    - what is cmake ?
    - why cmake ?
    - what does cross-platform means ?
    - cmake Stages
    - well known projects use cmake

- Part 2:
    - what is `CmakeLists.txt` ?
    - what is Cmake configure ?
    - what is Cmake build ?
    - what is `build` folder ?
    - what is the structure of the build folder ?
    - write a `helloworld.cpp` file that just print `helloworld`
    - use cmake to produce an executable target for this simple project

- Part 3:
    - static vs dynamic library
    - how to create static library in cmake ?
    - how to create dynamic library in cmake ?
    - write a simple calculator `calc.cpp` and `calc.hpp` that has add, sub, mul, div functionality
    - use cmake to create static/dynamic targets and link them with an executable that print the output of the calculator

- Part 4:
    - what is c++17 ?
    - what is macro in c++ ?
    - Project #1:
        - use `filesystem.hpp` from c++ std (it is found in c++17) to write `helloworld` to a text file.
        - use cmake to compile this project
    - Project #2:
        - remember the calculator we did in the previous step ?
        - send the input numbers of the calculator as macros using cmake (HINT: use -D when configure cmake)
        - then send these numbers from cmake to c++

- Part 5:
    - What is cmake scripting mode ?
    - write a cmake script that:
        - if in linux -> print `I'm in linux`
        - if in mac -> print `I'm in mac`
        - if in windows -> print `God bless us`
    - write a cmake script that print all even numbers from 1 to 1000000
    - create 2 variables, sum them and print the output
    - what is option() ?

- Part 6:
    - what is `add_subdirectory` ?
    - remember the example of the calculator ?
    - try to put `calc.cpp` and `calc.hpp` in their own folder called `calc`
    - create their own `CMakeLists.txt`
    - build, run and have fun (sometimes :D)

### Resources
* [CMake](https://cmake.org/cmake/help/v3.20/guide/tutorial/index.html)
* [Hitchhiker’s Guide](https://cgold.readthedocs.io/en/latest/index.html)
* [modern-cmake](https://cliutils.gitlab.io/modern-cmake/)
* [cmake-buildsystem](https://cmake.org/cmake/help/v3.4/manual/cmake-buildsystem.7.html)

### Quiz
* How to set a variable in CMake?
* How to print a message only when in release mode in CMake?

## Docker
Estimated Duration: 1 Day.

### Topics to learn:
* What is containerization?
* How is containerization implemented on Linux?
* Basic syntax of a Dockerfile.
* Difference between an image and a container in Docker.

### Resources that can help:
* https://www.youtube.com/watch?v=Gn7IoT_WSRA (This so happens to be the oldest ted-ed video that was ever created)
* https://www.youtube.com/watch?v=YFl2mCHdv24
* https://docker-curriculum.com/?fbclid=IwAR1C27VCbE1UGc1MjBWyKxdlnKXwQZwsch3UxIZRQQYjZERE1bsW1sItMIo

## Swig
Estimated Duration: 1 Day.

### Topics to learn:
* Why do we need swig?
* Swig directives.
* Typemaps.

### Quiz
* How to write a swig interface file?
* Why use separate interface files?

### Resources that can help:
* http://www.swig.org/Doc4.0/SWIGDocumentation.html#SWIG


## Graduation Project :smile:
Estimated Duration: 4 Days.
* Implement a Red Black Tree
* Use git to synchronize code among yourselves
* Write tests
* Write Continuous Integration Scripts (we use gitlab CI).
* Write Continuous Delivery Scripts
